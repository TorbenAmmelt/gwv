# GWV MAZE
This is the work of: 
* Torben Ammelt
* 
* 
* 
For the "Grundlagen der Wissensverarbeitung" course Tutorial 3 & 4
#Setup 

`git clone https://TorbenAmmelt@bitbucket.org/TorbenAmmelt/gwv.git`

#build and run

build with java

run `src.com.gwv.maze.Main.java`

#Interesting to look at
The main simply reads all inputs in /res 
There are 4 Interesting Parts:
* The Maze which is the main data structure
* The BreadthFirstSearch which is contains the search and representation logic it uses SearchResult,Vec2f and Path
* The MazeReaderService which reads a Maze file into our data structure
* And Last the Display which loops through the search