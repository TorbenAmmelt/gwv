package com.gwv.maze;

import com.gwv.maze.data.Maze;
import com.gwv.maze.display.Display;
import com.gwv.maze.file.MazeNotValidException;
import com.gwv.maze.file.MazeReaderService;
import com.gwv.maze.search.AStar;
import com.gwv.maze.search.BreadthFirstSearch;
import com.gwv.maze.search.DepthFirstSearch;
import com.gwv.maze.search.Search;

import java.io.File;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        File folder  = new File("prioRes");
        File resFile = new File("result");
        if (!resFile.exists()){
            resFile.mkdirs();
        }else {
            for (File f: resFile.listFiles()) {
                f.delete();
            }
        }
        for (File f: folder.listFiles()) {

            makeSearch(f,resFile);
        }
        File folder2  = new File("res");

        for (File f: folder2.listFiles()) {
            makeSearch(f,resFile);
        }
    }

    private static void makeSearch(File mazeFile,File resultFile) {
        System.out.println("reading file into maze:\n" + mazeFile.getAbsoluteFile());
        Maze maze;
        if (!mazeFile.exists())
        {
            throw new RuntimeException("could not find the given file");
        }
        try {
            maze = MazeReaderService.read(mazeFile);

            Search search = new BreadthFirstSearch(maze);
            Display.presentSearch(search,new File(resultFile,"BFS_"+mazeFile.getName()));


            System.out.println("search BFS finished for:\n" + mazeFile.getAbsoluteFile());
            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

            Search search3 = new AStar(maze);

            Display.presentSearch(search3,new File(resultFile,"AStern_"+mazeFile.getName()));


            System.out.println("search A-Star finished for:\n" + mazeFile.getAbsoluteFile());
            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");


            Search search2 = new DepthFirstSearch(maze);

            Display.presentSearch(search2,new File(resultFile,"DFS_"+mazeFile.getName()));


            System.out.println("search DFS finished for:\n" + mazeFile.getAbsoluteFile());
            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");


            }catch (IOException  e){
            e.printStackTrace();
        } catch (MazeNotValidException e) {
            e.printStackTrace();
        }
    }


}
