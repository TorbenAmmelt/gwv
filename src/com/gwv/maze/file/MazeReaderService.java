package com.gwv.maze.file;

import com.gwv.maze.data.Maze;
import com.gwv.maze.data.MazeState;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

/**
 * reads files and provides Mazes
 */
public class MazeReaderService {

    /**
     * no instances allowed
     */
    private MazeReaderService(){}

    /**
     *
     * @param f file of the maze
     * @return a Maze if the maze is valid or throws an exception
     */
    public static Maze read(File f) throws MazeNotValidException, IOException {
        List<String> lines = Files.readAllLines(f.toPath(), StandardCharsets.UTF_8);
        //counting x max size
        int maxX = 0;
        for (String line:lines) {
            maxX =  Math.max(line.length(),maxX);
        }
        maxX = maxX;
        int maxY = lines.size();
        MazeState[][] mazeStates= new MazeState[maxY][maxX];
        boolean mazeInvalid = false;
        for (int y = 0; y < lines.size(); y++) {
            String line = lines.get(y);
            for (int x = 0; x < line.length(); x++) {
                char c = line.charAt(x);
                switch (c){
                    case 'g':
                        mazeStates[y][x] = MazeState.GOAL;
                        break;
                    case 's':
                        mazeStates[y][x] = MazeState.START;
                        break;
                    case ' ':
                        mazeStates[y][x] = MazeState.EMPTY;
                        break;
                    case 'x':
                        mazeStates[y][x] = MazeState.WALL;
                        break;
                    default:
                        System.err.println("Maze is missing data");
                        mazeStates[y][x] = MazeState.WALL;
                        break;
                }
            }
        }
        if (mazeInvalid){
            throw new  MazeNotValidException();
        }
        return new Maze(mazeStates);
    }
}
