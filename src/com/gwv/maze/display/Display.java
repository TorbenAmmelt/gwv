package com.gwv.maze.display;

import com.gwv.maze.search.Search;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * renders the states of a search in a human readable form
 */
public class Display {
    public static void presentSearch(Search search, File f) throws IOException {
        f.delete();
        f.createNewFile();

        FileWriter fileWriter= new FileWriter(f);
        while (!search.hasEnded())
        {
            search.step();
            String step = search.stateRepresentation();
            System.out.println(step);
            fileWriter.append(step);
            fileWriter.append("\n");
        }
        System.out.println(search.bestSoFar().toString());
        fileWriter.append(search.bestSoFar().toString());
        fileWriter.append("\n");
        fileWriter.flush();
        fileWriter.close();

    }
}
