package com.gwv.maze.data;

import com.gwv.maze.data.graph.Node;
import com.gwv.maze.data.graph.Path;

public class SearchResult {
    Path path;
    int cost;

    public SearchResult(Path path, int cost) {
        this.path = path;
        this.cost = cost;
    }

    public int getCost() {
        return cost;
    }

    public Path getPath() {
        return path;
    }

    public Node getLast(){
        return path.getAsLinkedList().get(path.getAsLinkedList().size()-1);
    }

    public void add(Node n, int cost){
        path.add(n);
        this.cost+=cost;
    }

    public SearchResult copy(){
return        new SearchResult(path.copy(),cost);
    }

    @Override
    public String toString() {
        return "SearchResult{" +
                "cost=" + cost +
                ", path=" + path +
                '}';
    }
}
