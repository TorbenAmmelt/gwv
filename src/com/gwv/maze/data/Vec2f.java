package com.gwv.maze.data;

/**
 * this is a mathematical 2D-Vector class with 2 floats for x,y it offers a lot
 * of different operations and most of the time an static counterpart that does
 * not modify the vector and instead delivers a new one as a result
 * 
 * @see <a href="https://de.wikipedia.org/wiki/Vektor">Wikipedia</a>
 * @author Torben
 *
 */
public class Vec2f {
	public float x = 0;
	public float y = 0;

	/**
	 * creates a new Vector with x,y as a start value
	 * 
	 * @param x
	 * @param y
	 */
	public Vec2f(float x, float y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * 
	 * @return the x value of the vector
	 */
	public float x() {
		return x;
	}

	/**
	 * 
	 * @return the y value of the vector
	 */
	public float y() {
		return y;
	}

	/**
	 * sets the x value of the vector
	 * 
	 * @param x
	 */
	public void x(float x) {
		this.x = x;
	}

	/**
	 * sets the y value of the vector
	 * 
	 * @param y
	 */
	public void y(float y) {
		this.y = y;
	}

	/**
	 * @see <a href="https://en.wikipedia.org/wiki/Dot_product">Wikipedia</a>
	 * @param vec
	 * @return the dot product between this and vec
	 */
	public float scalarProd(Vec2f vec) {
		return x * vec.x + y * vec.y;
	}

	/**
	 * adds the given vec onto this one
	 * 
	 * @see <a href=
	 *      "https://en.wikipedia.org/wiki/Euclidean_vector#Addition_and_subtraction">Wikipedia</a>
	 * @param vec
	 * @return this
	 */
	public Vec2f add(Vec2f vec) {
		x = x + vec.x;
		y = y + vec.y;
		return this;
	}

	/**
	 * returns the vec1 + vec2
	 * 
	 * @see <a href=
	 *      "https://en.wikipedia.org/wiki/Euclidean_vector#Addition_and_subtraction">Wikipedia</a>
	 * @param vec1
	 * @param vec2
	 * @return vec1 + vec2
	 */
	public static Vec2f add(Vec2f vec1, Vec2f vec2) {
		return vec1.copy().add(vec2);
	}

	/**
	 * subs the given vec from this one
	 * 
	 * @see <a href=
	 *      "https://en.wikipedia.org/wiki/Euclidean_vector#Addition_and_subtraction">Wikipedia</a>
	 * @param vec
	 * @return this
	 */
	public Vec2f sub(Vec2f vec) {
		x = x - vec.x;
		y = y - vec.y;
		return this;
	}

	/**
	 * returns the vec1 - vec2
	 * 
	 * @see <a href=
	 *      "https://en.wikipedia.org/wiki/Euclidean_vector#Addition_and_subtraction">Wikipedia</a>
	 * @param vec1
	 * @param vec2
	 * @return vec1 - vec2
	 */
	public static Vec2f sub(Vec2f vec1, Vec2f vec2) {
		return vec1.copy().sub(vec2);
	}

	/**
	 * Scales this vector with the factor
	 * 
	 * @param factor
	 * @see <a href=
	 *      "https://en.wikipedia.org/wiki/Euclidean_vector#Scalar_multiplication">Wikipedia</a>
	 * 
	 * @return this
	 */
	public Vec2f scale(float factor) {
		float xF = x * factor;
		float yF = y * factor;
		return new Vec2f(xF, yF);
	}

	/**
	 * Scales the vector with the factor
	 * 
	 * @param factor
	 * @param vec
	 * @see <a href=
	 *      "https://en.wikipedia.org/wiki/Euclidean_vector#Scalar_multiplication">Wikipedia</a>
	 * @return factor*vec
	 */
	public static Vec2f scale(float factor, Vec2f vec) {
		return vec.copy().scale(factor);
	}

	/**
	 * returns the length^2 of the vector,<br>
	 * this is easier to perform then {@link Vec2f#length()} and should always be
	 * used over this method
	 * 
	 * @return length^2
	 */
	public float lengthSQ() {
		return x * x + y * y;
	}

	/**
	 * returns the length of the vector <br>
	 * If possible always use {@link Vec2f#lengthSQ()} over this, since it is easier
	 * to perform
	 * 
	 * @return length of the vector
	 */
	public float length() {
		return (float) Math.sqrt(lengthSQ());
	}

	/**
	 * returns vec with length 1, if its length is bigger than 0.000001F
	 */
	public Vec2f norm() {
		float length = length();
		if (length < 0.000001F) {
			return this;
		}
		return this.scale(1 / length());
	}

	/**
	 * returns a copy of vec with length 1, if its length is bigger than 0.000001F
	 */
	public static Vec2f norm(Vec2f vec) {
		return vec.copy().norm();
	}

	/**
	 * 
	 * 
	 * @see <a href="http://en.wikipedia.org/wiki/Vector_projection">Vector
	 *      projection</a>
	 * 
	 * @param vec
	 * @param vec1
	 * @return
	 */
	public static Vec2f vectorProjection(Vec2f vec, Vec2f vec1) {
		Vec2f temp = vec1.copy().norm();
		temp.scale(vec.scalarProd(vec) / vec1.length());
		return temp;
	}

	/**
	 * clones this Vector without that annoying stuff needed for clone()
	 */
	public Vec2f copy() {
		return new Vec2f(x, y);
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(x);
		result = prime * result + Float.floatToIntBits(y);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Vec2f other = (Vec2f) obj;
		// TODO change to modern version to compare 2 floats
		if (Float.floatToIntBits(x) != Float.floatToIntBits(other.x)) {
			return false;
		}
		if (Float.floatToIntBits(y) != Float.floatToIntBits(other.y)) {
			return false;
		}
		return true;
	}

	/**
	 *
	 * @return the vector with the values casted to int
	 */
	public Vec2f toInt()
	{
		x=(int)x;
		y=(int)y;
		return this;
	}

	/**
	 *
	 * @return a vector with the values of vec casted to int
	 */
	public static Vec2f toInt(Vec2f vec)
	{
		return vec.copy().toInt();
	}

	@Override
	public String toString() {
		return "Vec2f [x=" + x + ", y=" + y + "]" + ", length()=" + length();
	}

	/**
	 *
	 * @return a simpler String "[x,y]"
	 */
	public String toSimpleString() {
		return "["+ x + ","+ y + "]";
	}

	/**
	 *
	 * @return a simpler String "[x,y]" with x,y as int
	 */
	public String toSimpleIntString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append((int) this.x);
		sb.append(",");
		sb.append((int) this.y);
		sb.append("]");
		return sb.toString();
	}

	public Vec2f rotate(double rad) {
		float tempX = (float) (x * Math.cos(rad) - y * Math.sin(rad));
		float tempY = (float) (x * Math.sin(rad) + y * Math.cos(rad));
		x = tempX;
		y = tempY;
		return this;
	}

	public static Vec2f rotate(Vec2f vec, double rad) {
		return vec.copy().rotate(rad);
	}

	public float hamlitonDistance(){
		return Math.abs(x)+Math.abs(y);
	}

	public static float hamlitonDistance(Vec2f vec){
		return Math.abs(vec.x)+Math.abs(vec.y);
	}

}
