package com.gwv.maze.data;

/**
 * all possible maze states
 */
public enum MazeState {
    EMPTY, WALL, START, GOAL, VISITED, FRONTIER, PATH, CURRENT, PORTAL0, PORTAL1, PORTAL2, PORTAL3, PORTAL4, PORTAL5, PORTAL6, PORTAL7, PORTAL8, PORTAL9;

    @Override
    public String toString() {
        switch (this) {
            case WALL:
                return "◼";
            case GOAL:
                return "⚑";
            case START:
                return "☉";
            case EMPTY:
                return "☐";
            case VISITED:
                return "▼";
            case FRONTIER:
                return "▽";
            case PATH:
                return "☆";
            case CURRENT:
                return "★";
            case PORTAL0:
            case PORTAL1:
            case PORTAL2:
            case PORTAL3:
            case PORTAL4:
            case PORTAL5:
            case PORTAL6:
            case PORTAL7:
            case PORTAL8:
            case PORTAL9:
                return "●";
            default:
                return "ERROR: INVALID";
        }
    }

    public boolean isPortal() {
        switch (this) {
            case PORTAL0:
            case PORTAL1:
            case PORTAL2:
            case PORTAL3:
            case PORTAL4:
            case PORTAL5:
            case PORTAL6:
            case PORTAL7:
            case PORTAL8:
            case PORTAL9:
                return true;
            default:
                return false;
        }
    }
}
