package com.gwv.maze.data;

import com.gwv.maze.data.graph.Edge;
import com.gwv.maze.data.graph.Graph;
import com.gwv.maze.data.graph.Node;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * representation for a maze
 */
public class Maze {

    private final MazeState[][] mazeStates;
    private Vec2f startpos;
    private List<Vec2f> goals = new ArrayList<>();


    /**
     * @param mazeStates the maze states of the given maze
     */
    public Maze(MazeState[][] mazeStates) {
        this.mazeStates = mazeStates;
        int x = 0;
        int y = 0;
        for (MazeState[] mazeStatesX : mazeStates) {

            for (MazeState state : mazeStatesX) {
                if (state == MazeState.START) {
                    startpos = new Vec2f(x, y);
                }else if (state == MazeState.GOAL){
                    goals.add(new Vec2f(x, y));
                }
                x++;
            }
            x = 0;
            y++;
        }
    }

    /**
     * @param x 0 based coordinate of the maze counting the outer wall in
     * @param y 0 based coordinate of the maze counting the outer wall in
     * @return the maze state on the (x,y) position
     */
    public MazeState getMazeState(int x, int y) {
        return mazeStates[y][x];
    }

    /**
     * @param vec
     * @return the maze state on the (x,y) position represented by the vector as int
     */
    public MazeState getMazeState(Vec2f vec) {
        return getMazeState((int) vec.x, (int) vec.y);
    }

    /**
     * @param x
     * @param y
     * @param s
     */
    public void setMazeState(int x, int y, MazeState s) {
        mazeStates[y][x] = s;
    }

    /**
     * @param vec
     * @param s
     */
    public void setMazeState(Vec2f vec, MazeState s) {
        setMazeState((int) vec.x, (int) vec.y, s);
    }

    /**
     * @return the position of the start
     */
    public Vec2f getStartPos() {
        return startpos;
    }

    /**
     * @param pos a certain position
     * @return true if the given pos is a goal
     */
    public boolean isGoal(Vec2f pos) {
        return getMazeState(pos) == MazeState.GOAL;
    }

    /**
     * @param vec
     * @return the maze state on the (x,y) position represented by the vector as int
     */
    public List<Vec2f> getNeighbours(Vec2f vec) {
        if (getMazeState(vec).equals(MazeState.WALL)){
            return new LinkedList<Vec2f>();
        }
        LinkedList<Vec2f> neig = new LinkedList<Vec2f>(Arrays.asList(
                new Vec2f[]{
                        Vec2f.add(vec, new Vec2f(0, 1)),
                        Vec2f.add(vec, new Vec2f(1, 0)),
                        Vec2f.add(vec, new Vec2f(0, -1)),
                        Vec2f.add(vec, new Vec2f(-1, 0)),
                }));

        //remove invalid positions
        for (int i = 0; i < neig.size(); i++) {
            Vec2f neigbourPos = neig.get(i);
            //out of bounds
            if (neigbourPos.x < 0 || neigbourPos.x > mazeStates[0].length) {
                neig.remove(i);
                i--;
            } else if (neigbourPos.y < 0 || neigbourPos.y > mazeStates.length) {
                neig.remove(i);
                i--;
            }
            //wall is not walkable
            else if (getMazeState(neigbourPos) == MazeState.WALL) {
                neig.remove(i);
                i--;
            }
        }
        return neig;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        for (MazeState[] mazeStatesX : mazeStates) {
            for (MazeState mazeState : mazeStatesX) {
                sb.append(mazeState.toString());
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    public Maze copy() {
        MazeState[][] newMazeStates = new MazeState[mazeStates.length][mazeStates[0].length];
        int j = 0;
        for (MazeState[] array : mazeStates) {
            MazeState[] newArray = new MazeState[array.length];
            int i = 0;
            for (MazeState ms : array) {
                newArray[i] = ms;
                i++;
            }
            newMazeStates[j] = newArray;
            j++;
        }
        return new Maze(newMazeStates);
    }

    public Graph toGraph() {
        Graph ret = new Graph();
        //add Nodes
        for (int i = 0; i < mazeStates.length; i++) {
            MazeState[] mazeRow = mazeStates[i];
            for (int j = 0; j < mazeRow.length; j++) {
                Vec2f pos = new Vec2f(j, i);
                ret.addNode(new Node(pos,mazeRow[j]));
            }
        }
        //add edges
        for (int i = 0; i < mazeStates.length; i++) {
            MazeState[] mazeRow = mazeStates[i];
            for (int j = 0; j < mazeRow.length; j++) {
                Vec2f pos = new Vec2f(j, i);
                MazeState mazeState = mazeRow[j];

                List<Vec2f> neighbours = getNeighbours(pos);
                for (Vec2f neiPos : neighbours) {
                    if (mazeState.isPortal() && mazeState.equals(this.getMazeState(neiPos))) {
                        ret.addEdge(new Edge(ret.getNode(pos), ret.getNode(neiPos), 0));
                    } else {
                        ret.addEdge(new Edge(ret.getNode(pos), ret.getNode(neiPos), 1));
                    }
                }
            }
        }


        return ret;
    }

    public List<Vec2f> getGoals() {
        return goals;
    }
}
