package com.gwv.maze.data.graph;


public class Edge {
    private final Node first;
    private final Node second;
    private final float cost;

    public Edge(Node first, Node second, float cost) {
        this.first = first;
        this.second = second;
        this.cost = cost;
    }

    public Edge(Node first, Node second) {
        this(first,second,0);
    }

    public Node getFirst() {
        return first;
    }


    public Node getSecond() {
        return second;
    }


    public float getCost() {
        return cost;
    }

}
