package com.gwv.maze.data.graph;

import com.gwv.maze.data.MazeState;
import com.gwv.maze.data.Vec2f;

public class Node {
    final Vec2f pos;
    final MazeState mazeState;

    public Node(Vec2f pos,MazeState mazeState) {
        this.pos = pos;
        this.mazeState = mazeState;
    }

    public Vec2f getPos() {
        return pos;
    }

    public MazeState getMazeState() {
        return mazeState;
    }

    @Override
    public String toString() {
        return "Node{" +
                "pos=" + pos.toSimpleIntString() +
                ", mazeState=" + mazeState +
                '}';
    }
}
