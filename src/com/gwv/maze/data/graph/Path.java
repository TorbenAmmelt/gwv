package com.gwv.maze.data.graph;


import java.util.ArrayList;
import java.util.List;

public class Path {
    final List<Node> path;

    public Path(List<Node> path) {
        this.path = path;
    }



    public List<Node> getAsLinkedList() {
        return path;
    }

    public Path copy() {
        List<Node> copiedPath = new ArrayList<>();
        for (Node node : path) {
            copiedPath.add(node);
        }
        return new Path(copiedPath);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Path:");
        for (Node node: path) {
            sb.append(node.getPos().toSimpleIntString());
            sb.append("⇒");
        }
        return sb.toString();
    }

    public boolean contains(Node node){
       return  path.contains(node);
    }

    public void add(Node node){
        path.add(node);
    }
}
