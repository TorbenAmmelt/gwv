package com.gwv.maze.data.graph;

import com.gwv.maze.data.Vec2f;

import java.util.*;

public class Graph<getNeighbours> {
    HashMap<Vec2f,Node> nodes = new HashMap<>();
    LinkedList<Edge> edges = new LinkedList<Edge>();

    public Graph(LinkedList<Edge> edges, List<Node> nodes) {
        this.edges = edges;
        for (Node node: nodes) {
            this.nodes.put(node.pos,node);
        }
    }

    public Graph() {
    }

    public Collection<Node> getNodes() {
        return nodes.values();
    }


    public LinkedList<Edge> getEdges() {
        return edges;
    }



    public void addEdge(Edge e) {
        edges.add(e);
    }

    public void addNode(Node n) {
        nodes.put(n.pos,n);
    }

    public Node getNode(Vec2f pos) {
        return nodes.get(pos);
    }

    public List<Node> getNeighbours(Node node) {
        ArrayList<Node> ret = new ArrayList<Node>();
        for (Edge e : edges) {
            if (e.getFirst().equals(node)) {
                ret.add(e.getSecond());
            }
        }
        return ret;
    }

    public Edge getEdge(Node first, Node second) {
        for (Edge e : edges) {
            if (e.getFirst().equals(first) && e.getSecond().equals(second)) {
                return e;
            }
        }
        throw new IllegalArgumentException("there is no edge with this Nodes:" + first + "," + second);
    }
}
