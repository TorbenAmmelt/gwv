package com.gwv.maze.data.util;

import com.gwv.maze.data.graph.Node;

public class AStarPart {
    float hValue;
    float gValue;
    Node node;
    Node originNode;

    public AStarPart(float hValue, float gValue, Node node) {
        this.hValue = hValue;
        this.gValue = gValue;
        this.node = node;
    }
    public AStarPart(float hValue, float gValue, Node node,Node originNode) {
        this(hValue,gValue,node);
        this.originNode = originNode;
    }

    public float getValue() {
        return hValue+gValue;
    }

    public float gethValue() {
        return hValue;
    }

    public void sethValue(float hValue) {
        this.hValue = hValue;
    }

    public float getgValue() {
        return gValue;
    }

    public void setgValue(float gValue) {
        this.gValue = gValue;
    }

    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    public Node getOriginNode() {
        return originNode;
    }

    public void setOriginNode(Node originNode) {
        this.originNode = originNode;
    }
}
