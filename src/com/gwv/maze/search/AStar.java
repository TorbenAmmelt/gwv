package com.gwv.maze.search;


import com.gwv.maze.data.Maze;
import com.gwv.maze.data.SearchResult;
import com.gwv.maze.data.Vec2f;
import com.gwv.maze.data.graph.Edge;
import com.gwv.maze.data.graph.Graph;
import com.gwv.maze.data.graph.Node;
import com.gwv.maze.data.graph.Path;
import com.gwv.maze.data.util.AStarPart;
import com.sun.org.apache.xpath.internal.objects.XString;

import java.util.*;

public class AStar implements Search {

    HashMap<Node, AStarPart> open = new HashMap<>();
    HashMap<Node,AStarPart> closed = new HashMap<>();
    AStarPart last;
    Node startNode;
    private int step =0;
    private final Graph graph;
    private List<Vec2f> goals;

    public AStar(Maze maze) {
        graph = maze.toGraph();
        Vec2f startPos = maze.getStartPos();
        startNode = graph.getNode(startPos);
        last = new AStarPart(0, 0, startNode);
        open.put(startNode,last);
        goals = maze.getGoals();
    }

    @Override
    public void step() {
        if (!hasEnded()) {
            step++;
            Node lowest = getLowestNode(open);
            last = open.get(lowest);

            closed.put(lowest,last);
            open.remove(lowest);

            List<Node> neighbours = graph.getNeighbours(lowest);
            for (Node neighbour : neighbours) {
                if (!closed.containsKey(neighbour)) {
                    Edge edge = graph.getEdge(lowest, neighbour);

                    float newGValue = last.getgValue() + edge.getCost();
                    float heuristicValue = getHeuristicValue(neighbour.getPos());
                    AStarPart newSolution = new AStarPart(heuristicValue, newGValue, neighbour,lowest);

                    if (!open.containsKey(neighbour)) {
                        open.put(neighbour, newSolution);
                    } else {
                        AStarPart oldSolution = open.get(neighbour);
                        if (oldSolution.getValue()>newSolution.getValue()){
                            open.remove(neighbour);
                            open.put(neighbour, newSolution);
                        }
                    }
                }
            }
        } else {
            return;
        }

    }

    @Override
    public boolean hasEnded() {
        return goals.contains(last.getNode().getPos())|| open.isEmpty();
    }

    @Override
    public String stateRepresentation() {
        StringBuilder sb= new StringBuilder();
        sb.append("step: ");
        sb.append(step);
        sb.append("\n");
        sb.append("open: ");
        sb.append(stateRepresentation(open.keySet()));
        sb.append("\n");
        sb.append("closed: ");
        sb.append(stateRepresentation(closed.keySet()));
        return sb.toString();
    }

    private String stateRepresentation(Set<Node> keySet) {
        StringBuilder sb = new StringBuilder();
        for (Node n:keySet) {
            sb.append(n.getPos().toSimpleIntString());
        }
        return sb.toString();
    }

    @Override
    public SearchResult bestSoFar() {
        return new SearchResult(getPath(last), (int) last.getValue());
    }

    private Node getLowestNode(HashMap<Node, AStarPart> nodeAStarPartHashMap) {
        float min = Integer.MAX_VALUE;
        Node res = null;
        for (Map.Entry<Node, AStarPart> entry : nodeAStarPartHashMap.entrySet()) {
            AStarPart aStarPart = entry.getValue();
            Node node = entry.getKey();
            if (aStarPart.getValue() < min) {
                min = aStarPart.getValue();
                res = node;
            }
        }
        return res;
    }

    private float getHeuristicValue(Vec2f startPos) {
        float idealHeuristic = Float.MAX_VALUE;
        for (Vec2f goal : goals) {
            Vec2f diff = Vec2f.sub(startPos, goal);
            float hamiltonDistance = diff.hamlitonDistance();
            idealHeuristic = Math.min(idealHeuristic,hamiltonDistance);
        }
        return idealHeuristic;
    }

    private Path getPath(AStarPart aStarPart){

        List<Node> list = new ArrayList<>();
        list.add(aStarPart.getNode());

        AStarPart predecessorPart = aStarPart;
        Node predecessor = aStarPart.getOriginNode();
        while (predecessor!=null){
            list.add(predecessor);
            predecessorPart=closed.get(predecessor);
            predecessor= predecessorPart.getOriginNode();
        }
        return  new Path(list);
    }


}
