package com.gwv.maze.search;

import com.gwv.maze.data.Maze;
import com.gwv.maze.data.MazeState;
import com.gwv.maze.data.SearchResult;
import com.gwv.maze.data.Vec2f;
import com.gwv.maze.data.graph.Graph;
import com.gwv.maze.data.graph.Node;
import com.gwv.maze.data.graph.Path;

import java.util.*;


public class DepthFirstSearch implements Search {
    private final Graph graph;
    private final List<SearchResult> frontier;
    SearchResult bestSolution;
    private int stepCount;

    public DepthFirstSearch(Maze maze) {
        graph = maze.toGraph();
        frontier = new LinkedList<>();
        Vec2f startPos = maze.getStartPos();
        Node startNode = graph.getNode(startPos);
        LinkedList<Node> startPath = new LinkedList<>();
        startPath.add(startNode);
        bestSolution = new SearchResult(new Path(new LinkedList<>()),Integer.MAX_VALUE);
        frontier.add(new SearchResult(new Path(startPath),0));
    }

    public void step() {
        stepCount++;
        if (!frontier.isEmpty()) {
            SearchResult current = frontier.get(0);
            Node firstNode = current.getLast();
            frontier.remove(0);
            if (firstNode.getMazeState().equals(MazeState.GOAL)) {
                if(bestSolution.getCost()>current.getCost()){
                    bestSolution= current;
                }
            } else {
                List<Node> neighbours = graph.getNeighbours(firstNode);
                neighbours.stream()
                        .filter(n -> !current.getPath().contains(n))
                        .forEach(node -> {
                    boolean portalEdge = firstNode.getMazeState().isPortal() && firstNode.getMazeState().equals(node.getMazeState());

                    SearchResult newSearchresult = current.copy();
                    newSearchresult.add(node, portalEdge ? 0 : 1);
                    frontier.add(0,newSearchresult);

                });
            }
        } else {
            return;
        }
    }


    @Override
    public boolean hasEnded() {
        return frontier.isEmpty();
    }
    @Override
    public String stateRepresentation() {
        StringBuilder sb = new StringBuilder();
        sb.append("step:"+stepCount+" ");
        sb.append("size:"+frontier.size()+" ");
        sb.append(frontier.toString());
        return sb.toString();
    }

    @Override
    public SearchResult bestSoFar() {
        return bestSolution;
    }


}
