package com.gwv.maze.search;

import com.gwv.maze.data.SearchResult;

/**
 * interface for a search
 */
public interface Search {

    /**
     * make next step in the search process
     */
    public void step();

    /**
     * @return whether the search has ended
     */
    public boolean hasEnded();

    /**
     * @return the current state as a human readable output
     */
    public String stateRepresentation();

    public SearchResult bestSoFar();
}
