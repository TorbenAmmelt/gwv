package com.gwv.maze.search;

import com.gwv.maze.data.*;
import com.gwv.maze.data.graph.Node;
import com.gwv.maze.data.graph.Path;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * https://en.wikipedia.org/wiki/Breadth-first_search
 */
public class BreadthFirstSearch implements Search {

    Set<Vec2f> visited = new HashSet<>();//used to prevent circleing
    protected LinkedList<SearchResult> frontier = new LinkedList<SearchResult>();
    protected Maze maze;
    protected SearchResult current;
    protected int maxSize;
    protected int step;
    protected boolean solved=false;

    public BreadthFirstSearch(Maze maze) {
        this.maze = maze;
        LinkedList<Node> linked = new LinkedList<>();
        linked.add(new Node(maze.getStartPos(),maze.getMazeState(maze.getStartPos())));

        frontier.add(new SearchResult(new Path(linked), 0));
    }

    @Override
    public void step() {
        maxSize = Math.max(frontier.size(),maxSize);
        if (!frontier.isEmpty()) {
            current = frontier.getFirst();

            Vec2f currentPos = frontier.getFirst().getLast().getPos();
            frontier.removeFirst();
            visited.add(current.getLast().getPos());

            List<Vec2f> neighbourPositions = maze.getNeighbours(currentPos);
            for (Vec2f neighbourPos : neighbourPositions) {
                if (maze.isGoal(neighbourPos)) {
                    System.out.println("found way");
                    //since we do not use prices for moving we are done
                    frontier.clear();
                    solved=true;
                    break;
                } else {
                    if ( !current.getPath().getAsLinkedList().contains(neighbourPos)//does current path already contains node
                    && !visited.contains(neighbourPos) )//and there is no already known way to this node
                    {
                        List<Node> list = current.getPath().copy().getAsLinkedList();
                        list.add(new Node(neighbourPos,maze.getMazeState(neighbourPos)));
                        Path newPath = new Path(list);
                        int cost = current.getCost() + 1;
                        frontier.add(new SearchResult(newPath, cost));
                    }
                }
            }


        } else {
            System.out.println("search done, no way found");
        }
        step++;
    }

    @Override
    public boolean hasEnded() {
        return frontier.isEmpty();
    }

    @Override
    public String stateRepresentation() {
        StringBuilder sb = new StringBuilder();
        sb.append("STEP "+step+"\n");
        buildFrontier(sb);
        buildVisited(sb);

        sb.append("current cost = "+current.getCost()+"\n");

        buildMazePresentation(sb);

        if (frontier.isEmpty()) {
            if (solved){
                sb.append("search done path to goal was:\n" +
                        current.getPath().toString());
            }else {
                sb.append("search done no Path found\n");
            }

        }
        return sb.toString();
    }

    @Override
    public SearchResult bestSoFar() {
        return current;
    }

    private void buildMazePresentation(StringBuilder sb) {
        Maze mazePresentation = maze.copy();
        for (Vec2f vec : visited) {
            mazePresentation.setMazeState(vec, MazeState.VISITED);
        }
        for (SearchResult searchResult : frontier) {
            mazePresentation.setMazeState(searchResult.getLast().getPos(), MazeState.FRONTIER);
        }

        for (Node node : current.getPath().getAsLinkedList()) {
            mazePresentation.setMazeState(node.getPos(), MazeState.PATH);

        }

        Vec2f first = current.getLast().getPos();
        mazePresentation.setMazeState(first, MazeState.CURRENT);

        mazePresentation.setMazeState(maze.getStartPos(), MazeState.START);


        sb.append(mazePresentation);
    }

    private void buildVisited(StringBuilder sb) {
        sb.append("visited ("+ visited.size()+ "):");
        for (Vec2f vec : visited) {
            sb.append(vec.toSimpleIntString());
            sb.append(maze.getMazeState(vec).toString());
            sb.append(";");
        }
        sb.append("#\n");
    }


    private void buildFrontier(StringBuilder sb) {
        sb.append("frontier:");
        for (SearchResult searchResult : frontier) {
            Vec2f vec = searchResult.getLast().getPos();
            sb.append(vec.toSimpleIntString());
            sb.append(maze.getMazeState(vec).toString());
            sb.append(";");
        }
        sb.append("#\n");
        sb.append("frontierSize/frontierMaxSize:");
        sb.append(frontier.size());
        sb.append("/");
        sb.append(maxSize);
        sb.append("\n");
    }






}
